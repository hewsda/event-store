<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Adapter;

use Hewsda\EventStore\Stream\Stream;
use Hewsda\EventStore\Stream\StreamName;

interface Adapter
{
    public function create(Stream $stream): void;

    public function load(StreamName $streamName, int $minVersion = null): ?Stream;
}