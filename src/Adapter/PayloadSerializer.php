<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Adapter;

interface PayloadSerializer
{
    public function serializePayload(array $payload): string;

    public function unserializePayload(string $serialized): array;
}