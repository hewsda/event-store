<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Adapter\PayloadSerializer;

use Hewsda\EventStore\Adapter\PayloadSerializer;

class JsonPayloadSerializer implements PayloadSerializer
{

    public function serializePayload(array $payload): string
    {
        return json_encode($payload);
    }

    public function unserializePayload(string $serialized): array
    {
        return json_decode($serialized);
    }
}