<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Aggregate;

use Hewsda\EventStore\EventStore;
use Hewsda\EventStore\Stream\Stream;
use Hewsda\EventStore\Stream\StreamName;

class AggregateRepository
{
    /**
     * @var array
     */
    private $identityMap = [];

    /**
     * @var EventStore
     */
    private $eventStore;

    /**
     * @var AggregateType
     */
    private $aggregateType;

    public function __construct(EventStore $eventStore, AggregateType $aggregateType)
    {
        $this->eventStore = $eventStore;
        $this->aggregateType = $aggregateType;
    }

    public function addAggregateRoot($eventSourcedAggregateRoot)
    {
        $this->assertAggregateType($eventSourcedAggregateRoot);

        $domainEvents = $eventSourcedAggregateRoot->popRecordedEvents();
        $aggregateId = $eventSourcedAggregateRoot->aggregateId();

        $stream = new Stream($this->determineEventStreamName($aggregateId), new \ArrayIterator($domainEvents));

        $this->eventStore->create($stream);
    }

    public function getAggregateRoot(string $aggregateId)
    {
        if (isset($this->identityMap[$aggregateId])) {
            return $this->identityMap[$aggregateId];
        }

        $streamName = $this->determineEventStreamName($aggregateId);

        $sourced = $this->eventStore->load($streamName);

        $this->identityMap[$aggregateId] = $sourced;

        return $sourced;
    }

    protected function determineEventStreamName(string $aggregateId): StreamName
    {
        return new StreamName($this->aggregateType->toString() . '-' . $aggregateId);
    }

    protected function assertAggregateType($eventSourcedAggregateRoot)
    {
        $this->aggregateType->assert($eventSourcedAggregateRoot);
    }
}