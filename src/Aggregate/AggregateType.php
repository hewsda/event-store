<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Aggregate;

use Hewsda\EventStore\Exception\InvalidArgumentException;

class AggregateType
{
    /**
     * @var string
     */
    private $aggregateType;

    public static function fromString($aggregateType): self
    {
        return new static($aggregateType);
    }

    public static function fromAggregateRootClass(string $aggregateRoot): self
    {
        if (!class_exists($aggregateRoot)) {
            throw new \InvalidArgumentException(
                sprintf('Aggregate root class "%s" does not exists', $aggregateRoot));
        }

        return new static($aggregateRoot);
    }

    public static function fromAggregateRoot($eventSourced): self
    {
        //todo type provider to extend root
        if (!is_object($eventSourced)) {
            throw new InvalidArgumentException('Aggregate root must be an object.');
        }

        return new static(get_class($eventSourced));
    }

    public function assert($aggregateRoot)
    {
        $otherAggregateType = self::fromAggregateRoot($aggregateRoot);

        if (! $this->equals($otherAggregateType)) {
            throw new InvalidArgumentException(
                sprintf('Aggregate types must be equal. %s != %s',
                    $this->toString(), $otherAggregateType->toString())
            );
        }
    }

    public function toString(): string
    {
        return $this->aggregateType;
    }

    public function __toString(): string
    {
        return $this->toString();
    }

    public function equals(AggregateType $aType): bool
    {
        return $this->toString() === $aType->toString();
    }

    private function __construct(string $aggregateType)
    {
        if (empty($aggregateType)) {
            throw new InvalidArgumentException('Aggregate type can not be empty.');
        }

        $this->aggregateType = $aggregateType;
    }
}