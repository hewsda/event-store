<?php

declare(strict_types=1);

namespace Hewsda\EventStore;

use AppendIterator;
use Hewsda\EventStore\Adapter\Adapter;
use Hewsda\EventStore\Adapter\Feature\CanHandleTransaction;
use Hewsda\EventStore\Exception\RuntimeException;
use Hewsda\EventStore\Stream\Stream;
use Hewsda\EventStore\Stream\StreamName;
use Prooph\Common\Event\ActionEventEmitter;

class EventStore
{
    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var ActionEventEmitter
     */
    protected $emitter;

    /**
     * @var \Iterator
     */
    protected $recordedEvents;

    /**
     * @var bool
     */
    protected $inTransaction = false;

    /**
     * EventStore constructor.
     *
     * @param Adapter $adapter
     * @param ActionEventEmitter $emitter
     */
    public function __construct(Adapter $adapter, ActionEventEmitter $emitter)
    {
        $this->adapter = $adapter;
        $this->emitter = $emitter;
        $this->recordedEvents = new \AppendIterator();
    }

    public function create(Stream $stream): void
    {
        $argv = ['stream' => $stream];

        $event = $this->emitter->getNewActionEvent(__FUNCTION__ . '.pre', $this, $argv);
        $this->emitter->dispatch($event);

        if ($event->propagationIsStopped()) {
            return;
        }

        if (!$this->inTransaction) {
            throw new RuntimeException('Stream creation failed. EventStore is not in an active transaction');
        }

        $stream = $event->getParam('stream');

        $this->adapter->create($stream);

        $this->recordedEvents->append($stream->streamEvents());
        $event->setName(__FUNCTION__ . '.post');

        $this->emitter->dispatch($event);
    }

    public function load(StreamName $streamName, $minVersion = null): Stream
    {
        $argv = ['streamName' => $streamName, 'minVersion' => $minVersion];
        $event = $this->emitter->getNewActionEvent(__FUNCTION__ . '.pre', $this, $argv);

        $this->getEmitter()->dispatch($event);

        if ($event->propagationIsStopped()) {
            $stream = $event->getParam('stream', false);
            if ($stream instanceof Stream && $stream->streamName()->toString() === $streamName->toString()) {
                return $stream;
            }

            $this->raiseStreamNotFoundException($stream->streamName());
        }

        $streamName = $event->getParam('streamName');
        $minVersion = $event->getParam('minVersion');

        $stream = $this->adapter->load($streamName, $minVersion);

        if (!$stream) {
            $this->raiseStreamNotFoundException($streamName->toString());
        }

        $event->setName(__FUNCTION__ . '.post');
        $event->setParam('stream', $stream);

        $this->getEmitter()->dispatch($event);

        if ($event->propagationIsStopped()) {
            $this->raiseStreamNotFoundException($streamName);
        }

        return $event->getParam('stream');
    }

    public function commit(): void
    {
        if (!$this->inTransaction) {
            throw new RuntimeException('Cannot commit transaction. EventStore has no active transaction');
        }

        $event = $this->getEmitter()->getNewActionEvent(__FUNCTION__ . '.pre', $this);
        $event->setParam('inTransaction', true);

        $this->getEmitter()->dispatch($event);

        if ($event->propagationIsStopped()) {
            $this->rollback();
            return;
        }

        $this->inTransaction = false;

        if ($this->adapter instanceof CanHandleTransaction) {
            $this->adapter->commit();
        }

        $event = $this->getEmitter()->getNewActionEvent(
            __FUNCTION__ . '.post',
                $this,
                ['recordedEvents' => $this->recordedEvents]
        );

        $this->recordedEvents = new AppendIterator();

        $this->getEmitter()->dispatch($event);
    }

    public function transactional(callable $callable)
    {
        $this->beginTransaction();

        try {
            $result = $callable($this);
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();

            throw $e;
        }
        return $result ?? true;
    }

    public function beginTransaction(): void
    {
        if (!$this->inTransaction && $this->adapter instanceof CanHandleTransaction) {
            $this->adapter->beginTransaction();
        }

        $this->inTransaction = true;

        $event = $this->getEmitter()->getNewActionEvent(
            __FUNCTION__,
            $this,
            ['inTransaction' => true]
        );

        $this->getEmitter()->dispatch($event);
    }

    public function rollback(): void
    {
        if (!$this->inTransaction) {
            throw new RuntimeException('Cannot rollback transaction. EventStore has no active transaction');
        }

        if (!$this->adapter instanceof CanHandleTransaction) {
            throw new RuntimeException('Adapter cannot handle transaction and therefore cannot rollback');
        }

        $this->adapter->rollback();

        $this->inTransaction = false;

        $event = $this->getEmitter()->getNewActionEvent(__FUNCTION__, $this);

        $this->emitter->dispatch($event);

        $this->recordedEvents = new AppendIterator();
    }

    public function isInTransaction(): bool
    {
        return $this->inTransaction;
    }

    public function getEmitter(): ActionEventEmitter
    {
        return $this->emitter;
    }

    private function raiseStreamNotFoundException(StreamName $streamName): RuntimeException
    {
        throw new RuntimeException(
            sprintf(
                'A stream with name %s could not be found',
                $streamName->toString()
            )
        );
    }
}