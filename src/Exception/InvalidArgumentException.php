<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements EventStoreException
{

}