<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Exception;

class RuntimeException extends \RuntimeException implements EventStoreException
{

}