<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Stream;

class Stream
{
    /**
     * @var StreamName
     */
    protected $streamName;

    /**
     * @var \Iterator
     */
    protected $streamEvents;

    /**
     * Stream constructor.
     *
     * @param StreamName $streamName
     * @param \Iterator $streamEvents
     */
    public function __construct(StreamName $streamName, \Iterator $streamEvents)
    {
        $this->streamName = $streamName;
        $this->streamEvents = $streamEvents;
    }

    public function streamName(): StreamName
    {
        return $this->streamName;
    }

    public function streamEvents(): \Iterator
    {
        return $this->streamEvents;
    }
}