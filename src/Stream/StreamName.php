<?php

declare(strict_types=1);

namespace Hewsda\EventStore\Stream;

use Assert\Assertion;

class StreamName
{
    private $name;

    /**
     * StreamName constructor.
     * @param string $name
     *
     * @throws \Assert\AssertionFailedException
     */
    public function __construct($name)
    {
        Assertion::string($name, 'StreamName must be a string');
        Assertion::notEmpty($name, 'StreamName must not be empty');
        Assertion::maxLength($name, 200, 'StreamName should not be longer than 200 chars');

        $this->name = $name;
    }

    public function toString(): string
    {
        return $this->name;
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}