<?php

declare(strict_types=1);

namespace Hewsda\CacheStoreAdapter;

use Hewsda\EventStore\Adapter\Adapter;
use Hewsda\EventStore\Adapter\Feature\CanHandleTransaction;
use Hewsda\EventStore\Stream\Stream;
use Hewsda\EventStore\Stream\StreamName;
use Illuminate\Contracts\Cache\Repository;

class EventStoreCacheAdapter implements Adapter, CanHandleTransaction
{
    /**
     * @var [\Iterator]
     */
    private $streams;

    /**
     * @var Repository
     */
    private $store;

    /**
     * EventStoreCacheAdapter constructor.
     *
     * @param Repository $store
     */
    public function __construct(Repository $store)
    {
        $this->store = $store;
    }

    public function create(Stream $stream): void
    {
        //need in memory to be in transaction
        // or configure eventStore

        $streamEvents = $stream->streamEvents();

        $streamEvents->rewind();

        $this->streams[$stream->streamName()->toString()] = $streamEvents;
    }

    public function load(StreamName $streamName, int $minVersion = null): ?Stream
    {
        if ($this->store->has($streamName->toString())) {
            $streamEvents = $this->store->get($streamName->toString());

            $streamEvents->rewind();

            return new Stream($streamName, $streamEvents);
        }

        return null;
    }

    public function commit(): void
    {
        $this->cache->forever(key($this->streams), current($this->streams));
    }

    public function beginTransaction(): void
    {

    }

    public function rollback(): void
    {
        if ($this->streams) {
            $this->cache->forget(key($this->streams));
        }

        $this->streams = null;
    }
}